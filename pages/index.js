import { useEffect } from "react";
import HeaderSeo from "../HeaderSeo/HeaderSeo";

import { HeadSeo } from "../data/Seo";

export default function Home() {
  useEffect(() => {
    window.location = "https://xoanen.surge.sh/";
  }, []);
  return (
    <>
      <HeaderSeo
        title={HeadSeo.title}
        desc={HeadSeo.desc}
        image={HeadSeo.image}
      />
      <div></div>
    </>
  );
}
